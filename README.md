# README #

This repository contains the Java code of an example of an executable statechart created with the Yakindu statechart tool. With the exception of the file ChronoMain::ChronometerMain.java
all other Java files have been automatically generated with Yakindu.

A YouTube video explaining everything can be found on 
https://www.youtube.com/watch?v=XJPivUX58gk

More related videos are available in the YouTube playlist
https://www.youtube.com/playlist?list=PLmHMvhX5wK_aohX5sOeAMogFDwlc3gJYR

This code is part of a tutorial created in the context of a software modelling course taught by Tom Mens at the University of Mons, Belgium.