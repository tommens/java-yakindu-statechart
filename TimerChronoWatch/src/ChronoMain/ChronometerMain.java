package ChronoMain;

	  import org.yakindu.scr.TimerService;
	  import org.yakindu.scr.chronometer.ChronometerStatemachine;
	  public class ChronometerMain {
		  
		  public static void printingTimer(ChronometerStatemachine sm) {
              if (sm.getSCIC().getRing()) {
            	  System.out.println("... ringing ...");
            	  }
              else {
	               System.out.println("The value of memTimer =" + sm.getSCIC().getMemTimer());
	               System.out.println("The value of timer =" + sm.getSCIC().getTimer());
	               }
		  }

		  public static void printingStopwatch(ChronometerStatemachine sm) {
              if (sm.getSCIC().getRing()) {
            	  System.out.println("... ringing ...");
            	  }
              else {
	               System.out.println("The value of totalTime =" + sm.getSCIC().getTotalTime());
	               System.out.println("The value of lapTime =" + sm.getSCIC().getLapTime());
	               }
		  }
		  
	        public static void main(String[] args) throws Exception {
	               ChronometerStatemachine sm = new ChronometerStatemachine();
	               sm.setTimer(new TimerService());
	               // enter the sm and activate the Idle state
	               sm.init();
	               sm.enter();
	               
                   System.out.println("*** START INITIALISING MEMTIMER ***");
	               sm.getSCIC().raiseRight();
	               // increment memTimer in increments of 1
	               for (int i = 0; i < 3; i++) {
                       Thread.sleep(1000);
                       sm.runCycle();
                       printingTimer(sm);
 	               }
	               // increment memTimer once with increment of 5
	               sm.getSCIC().raiseUp();
                   sm.runCycle();
                   printingTimer(sm);
	               // return to Idle state 
	               sm.getSCIC().raiseRight();
                   sm.runCycle();
	               // start running timer (T_Active state)
                   System.out.println("*** START RUNNING TIMER ***");
	               sm.getSCIC().raiseUp();
                   sm.runCycle();
                   printingTimer(sm);
	               for (int i = 0; i < 10; i++) {
                       Thread.sleep(1000);
                       sm.runCycle();
                       printingTimer(sm);
	               }
	               System.out.println("*** SWITCHING TO STOPWATCH ***");
	               sm.getSCIC().raiseLeft();
                   sm.runCycle();
                   printingStopwatch(sm);
	               sm.getSCIC().raiseUp();
	               for (int i = 0; i < 5; i++) {
                       Thread.sleep(1000);
                       sm.runCycle();
                       printingStopwatch(sm);
	               }
	               System.out.println("*** STOPWATCH LAPTIME MODE ***");
	               sm.getSCIC().raiseUp();
                   sm.runCycle();
	               for (int i = 0; i < 2; i++) {
                       Thread.sleep(1000);
                       sm.runCycle();
                       printingStopwatch(sm);
	               }
	               System.out.println("*** STOPWATCH NORMAL MODE ***");
	               sm.getSCIC().raiseUp();
                   sm.runCycle();
	               for (int i = 0; i < 2; i++) {
                       Thread.sleep(1000);
                       sm.runCycle();
                       printingStopwatch(sm);
	               }
                   
	               System.out.println("*** SWITCHING BACK TO TIMER ***");
	               sm.getSCIC().raiseLeft();
                   sm.runCycle();
                   printingStopwatch(sm);
	               
	         }
	   }

